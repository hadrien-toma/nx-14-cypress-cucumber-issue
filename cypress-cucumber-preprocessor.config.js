const { readFileSync, mkdirSync } = require('fs');
const { resolve, join } = require('path');

const processCwd = process.cwd();
const projectJsonAsString = readFileSync(join(processCwd, `project.json`));
const projectJson = JSON.parse(projectJsonAsString);

const stepDefinitionsPath = `./src/e2e/**/*.ts`;
const outputFolderPath = resolve(`${processCwd}/${'../'.repeat(projectJson.sourceRoot.split('/').length - 1)}dist/cypress/${projectJson.sourceRoot.replace('/src', '')}/cucumber`);

mkdirSync(outputFolderPath, { recursive: true });

module.exports = {
	nonGlobalStepDefinitions: true,
	stepDefinitions: stepDefinitionsPath,
	cucumberJson: {
		generate: true,
		outputFolder: outputFolderPath,
		filePrefix: '',
		fileSuffix: '.feature',
	},
};
