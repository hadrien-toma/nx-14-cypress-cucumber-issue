import { Before, Then } from '@badeball/cypress-cucumber-preprocessor';
import { theUserIsLoggedIn } from '../steps';

Before(() => {
	cy.visit('/');
});

//#region    call internal Cucumber dependencies
theUserIsLoggedIn();
//#endregion call internal Cucumber dependencies

Then('the application is tested', () => {
	expect(undefined).to.equal(undefined);
});
