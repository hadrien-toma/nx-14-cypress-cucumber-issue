import { Before, Given } from '@badeball/cypress-cucumber-preprocessor';

Before(() => {
	cy.server();
});

export function theUserIsLoggedIn() {
	Given('the user is logged in as {string} password: {string}', (login: string, password: string) => {
		cy.login(login, password);
	});
}
