import { addCucumberPreprocessorPlugin } from "@badeball/cypress-cucumber-preprocessor";
import * as webpack from "@cypress/webpack-preprocessor";
import { defineConfig } from "cypress";

async function setupNodeEvents(
	on: Cypress.PluginEvents,
	config: Cypress.PluginConfigOptions
): Promise<Cypress.PluginConfigOptions> {
	await addCucumberPreprocessorPlugin(on, config);

	on(
		"file:preprocessor",
		webpack({
			webpackOptions: {
				resolve: {
					extensions: [".ts", ".js"],
				},
				module: {
					rules: [
						{
							test: /\.ts$/,
							exclude: [/node_modules/],
							use: [
								{
									loader: "ts-loader",
								},
							],
						},
						{
							test: /\.feature$/,
							use: [
								{
									loader: "@badeball/cypress-cucumber-preprocessor/webpack",
									options: {
										...config,
										"nonGlobalStepDefinitions": true
									},
								},
							],
						},
					],
				},
			},
		})
	);

	// Make sure to return the config object as it might have been modified by the plugin.
	return config;
}

export default defineConfig({
	e2e: {
		chromeWebSecurity: false,
		fileServerFolder: '.',
		fixturesFolder: './src/fixtures',
		screenshotsFolder: "../../../../dist/cypress/apps/my/directory/my-app-e2e/screenshots",
		setupNodeEvents,
		specPattern: "**/*.feature",
		supportFile: "./src/support/e2e.ts",
		video: true,
		videosFolder: "../../../../dist/cypress/apps/my/directory/my-app-e2e/videos",
	},
});